drop table UTILISATEURGL;
drop table Forum cascade constraints;
drop table ALERTE cascade constraints;
drop table MESSAGE cascade constraints; 
drop table NOTIFICATION cascade constraints;
drop table DEMANDE_RV cascade constraints;
drop table DONNEUR cascade constraints;

/*Modifier*/
CREATE TABLE DONNEUR(
    NUM_DONNEUR NUMBER(5) PRIMARY KEY,
    NOM VARCHAR2(30),
    PRENOM  VARCHAR2(30),
    ADRESSE VARCHAR2(50),
    NUM_TEL integer,
    Adresse_mail VARCHAR(20) unique,
    REMARQUE VARCHAR(20),
    DATE_NAISSANCE VARCHAR2(20),
    GROUPE_SANGUIN VARCHAR2(4),
    RHESUS CHAR,
    genre VARCHAR(20), 
    image varchar(50),
    CHECK  (GROUPE_SANGUIN IN('A','B','AB','O')),
    CHECK  (RHESUS IN('-','+'))
);

CREATE TABLE UTILISATEURGL(
    email VARCHAR(20) PRIMARY key,
    password VARCHAR(20),
    type_user VARCHAR(20)
);

CREATE TABLE DEMANDE_RV(
    NUM_DONNEUR NUMBER(5) REFERENCES DONNEUR,
    DATE_RV VARCHAR2(20),
    Heure VARCHAR2(6),
    etat Varchar(20) DEFAULT 'Acceptee',
    PRIMARY KEY (NUM_DONNEUR,DATE_RV)
);

CREATE TABLE MESSAGE(
    NUM_EMETTEUR VARCHAR2(30) ,
    DATE_MESSAGE VARCHAR2(20),
    CONTENU_MESSAGE  CHAR(500),
    NUM_RECEPTEUR NUMBER(3) REFERENCES Donneur(Num_donneur),
    PRIMARY KEY( NUM_EMETTEUR,NUM_RECEPTEUR,DATE_MESSAGE)
);

CREATE TABLE NOTIFICATION (
    NUM_EMETTEUR VARCHAR2(30) , /*L'adminstrateur qui envoi les notifs et les alertes*/
    DATE_NOTIF VARCHAR2(20),
    CONTENU_NOTIF  VARCHAR2(100),
    PRIMARY KEY( NUM_EMETTEUR,DATE_NOTIF)
);

CREATE TABLE ALERTE(
    NUM_EMETTEUR VARCHAR2(30) ,
    DATE_ALERTE VARCHAR2(20),
    CONTENU_ALERTE  VARCHAR2(100),
    PRIMARY KEY( NUM_EMETTEUR,DATE_ALERTE)
);

Create Table Forum(
    Message VARCHAR(500),
    NUM_DONNEUR NUMBER(5) REFERENCES DONNEUR,
    DATE_Message VARCHAR2(30),
    PRIMARY KEY( NUM_DONNEUR,DATE_Message)
);


INSERT INTO DONNEUR VALUES(1,'Rehman','Belaid','ALGER','098738387','rehmanb@gmail.com','RIEN','01/01/2000','A','-','Homme','D:\Cours\Cours L3\GL2\Binome1.jpg');
INSERT INTO DONNEUR VALUES(2,'Tiki','Fatima Zahra','ALGER','0763537229','tikifz@gmail.com','RIEN','01/01/1995','O','+','Femme','D:\Cours\Cours L3\GL2\Binome1.jpg');
INSERT INTO UTILISATEURGL VALUES('tikifz@gmail.com','0000','Donneur');
INSERT INTO UTILISATEURGL VALUES('rehmanb@gmail.com','0000','Donneur');
INSERT INTO UTILISATEURGL VALUES('user@gmail.com','1111','Admin');
INSERT INTO DEMANDE_RV VALUES(1,'25/03/2021','12h20','Acceptee');
INSERT INTO DEMANDE_RV VALUES(1,'02/01/2020','18h45','Acceptee');
INSERT INTO DEMANDE_RV VALUES(1,'02/07/2020','8h15','Refusee');
INSERT INTO DEMANDE_RV VALUES(2,'02/01/2020','16h25','Refusee');
INSERT INTO DEMANDE_RV VALUES(2,'12/04/2017','10h05','Attente');
INSERT INTO DEMANDE_RV VALUES(2,'19/10/2019','14h13','Attente');
INSERT INTO MESSAGE VALUES('email1','28/02/2019','Bonjour,comment ca va?',2);
INSERT INTO MESSAGE VALUES('email1','28/02/2019','Ca va bien et toi?',1);
INSERT INTO NOTIFICATION VALUES('email1','24/03/2021','Rappel: Votre Rendez-Vous est prevu demain');
INSERT INTO NOTIFICATION VALUES('email1','27/03/2021','Votre don a ete bien enregitre');
INSERT INTO NOTIFICATION VALUES('email1','01/01/2020','Rappel: Votre Rendez-Vous est prevu demain');
INSERT INTO NOTIFICATION VALUES('email1','10/04/2017','L''organisation des dons prevu pour le 11 est annulee');
INSERT INTO NOTIFICATION VALUES('email1','18/10/2019','Rappel: Votre Rendez-Vous est prevu demain');
INSERT INTO ALERTE VALUES('email1','03/01/2020','Vous avez rate votre rendez vous');
INSERT INTO Forum VALUES('Bonjour,C''est pour quand la prochaine organisation des dons?',1,'28/02/2019');
INSERT INTO Forum VALUES('Bonjour,y''a t-il encore des rdv',2,'05/02/2017');

Commit;












