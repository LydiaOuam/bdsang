package com.company;

import net.proteanit.sql.DbUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CarnetSante extends JPanel {

    private JTable rdv = new JTable();
    private JScrollPane rdvPan = new JScrollPane(rdv);
    private JEditorPane remq = new JEditorPane();

    private JButton actua = new JButton("Actualiser");

    private JLabel nom = new JLabel(),
                   prenom = new JLabel(),
                   der_rdv = new JLabel(),
                   rdv_ = new JLabel(),
                   titre = new JLabel("<html><h1><u>Carnet de sante</u></h1></html>");

    JLabel lbl[] = {new JLabel("Nom :"),new JLabel("Prenom :"),new JLabel("Dernier RDV:"),
        new JLabel("Rendez-Vous:"),new JLabel("Remarque :"),};
    JLabel lbl_affichage[] = {nom,prenom,der_rdv,rdv_};


    private void Afficher_rdv(String Num_donneur,Connection Conn) {
        try {
            ResultSet rs;
            rs = Conn.createStatement().executeQuery("select * from Demande_RV Where NUM_DONNEUR=" + Num_donneur);
            this.rdv.setModel(DbUtils.resultSetToTableModel(rs));
            //System.out.println(Num_donneur);
            rs = Conn.createStatement().executeQuery("select * from Donneur Where NUM_DONNEUR=" + Num_donneur);

            if (rs.next()) {
                nom.setText(rs.getString(2));
                prenom.setText(rs.getString(3));
                rdv_.setText(rs.getString(2));
                remq.setText(rs.getString(7));
                rs = Conn.createStatement().executeQuery("select * from Demande_RV Where NUM_DONNEUR=" + Num_donneur);
                rs.next();
                der_rdv.setText(rs.getString(2));

            }
        } catch (Exception z) {
           /* JOptionPane.showMessageDialog(null, "Alerter les administrateur",
                    "Erreur 404", JOptionPane.WARNING_MESSAGE);*/
           z.printStackTrace();
        }
    }
    public CarnetSante(int x, int y, int hauteur, int longueur, String Num_donneur, Connection Conn) {

        Afficher_rdv(Num_donneur,Conn);


        setSize(longueur, hauteur);
        this.setLayout(null);
        this.add(titre);
        this.titre.setBounds((longueur/2)+50,20,600,90);
        this.titre.setFont(new java.awt.Font("Monospaced", 1, 40));


        this.add(rdvPan);

        int i= 0;
        for(JLabel l : lbl){
            this.add(l);
            l.setBounds(100, 200+(40*i), 100, 30);
            i++;
        }

        /******************************************************************************/
        i=0;
        for(JLabel l : lbl_affichage){
            this.add(l);
            l.setBounds(180, 200+(40*i), 200, 30);
            l.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.BLACK));
            l.setBackground(null);
            i++;
        }

        this.add(remq);
        this.remq.setBounds(180, 360, 200, 100);;
        remq.setEditable(false);

        this.add(rdvPan);
        this.rdvPan.setBounds(500,200,400,300);
        this.rdvPan.setBackground(Color.GRAY);

        this.add(actua);
        this.actua.setBounds(800,160,100,30);
        this.actua.setBackground(Color.WHITE);
        this.actua.setForeground(Color.BLACK);
/*
        try {
            ResultSet rs;
            rs = Conn.createStatement().executeQuery("select * from Donneur Where NUM_DONNEUR="+Num_donneur);
            if(rs.next()){
                nom.setText(rs.getString(2));
                prenom.setText(rs.getString(3));
                der_rdv.setText(rs.getString(2));

                .setText(rs.getString(2));
                remq.setText(rs.getString(7));
            }
        }catch(Exception e) {
            JOptionPane.showMessageDialog(null, "Alerter les administrateur",
                    "Erreur 404", JOptionPane.WARNING_MESSAGE);
        }
*/
        actua.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Afficher_rdv(Num_donneur,Conn);
            }
        });
        this.setVisible(false);

    }
}
