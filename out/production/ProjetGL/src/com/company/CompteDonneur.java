package com.company;

import net.proteanit.sql.DbUtils;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;

public class CompteDonneur extends JPanel {


    JLabel l,titre =new JLabel("<html></u><p style= font-size:20px>Mon Compte</p></u></html>");
    private JTextField textField_1 = new JTextField(),
            textField_2 = new JTextField(),
            textField_3 = new JTextField(),
            textField_4 = new JTextField(),
            textField_5 = new JTextField(),
            textField_6 = new JTextField(),
            textField_7 = new JTextField(),
            textField_8 = new JTextField(),
            type_sang   = new JTextField(),
            type_rs     = new JTextField();

    private JTextField[] tab_field =  {textField_1,textField_2,textField_3,textField_4,textField_5,textField_6,
            textField_7,textField_8,type_sang,type_rs};

    private JLabel[] texts = {new JLabel("Code Donneur"),new JLabel("Nom"),new JLabel("Prenom"),
            new JLabel("Adresse"),
            new JLabel("Numero telephone:"),new JLabel("Adresse mail:"),
            new JLabel("Date de naissance"),
            new JLabel("Groupe sang"),new JLabel("Rhesus"),
            new JLabel("Genre")};
    private JLabel lbl_remq = new JLabel("Remarques:");

    private JButton sauvgarder = new JButton("Sauvgarder"),modifier = new JButton("Modifier");

    private JEditorPane remarque = new JEditorPane();
    private String path;
    @SuppressWarnings("unchecked")
    public CompteDonneur(int x, int y, int hauteur, int longueur, String Num_donneur, Connection Conn) {

        this.add(titre);
        this.titre.setBounds(longueur/2,30,300,50);

        JButton btn = new JButton("Parcourir");
        btn.setBounds(625, 300, 100, 29);
        btn.setEnabled(false);
        btn.setBackground(Color.WHITE);
        btn.setForeground(Color.BLACK);
        this.add(btn);

        JPanel Photo = new JPanel();
        Photo.setBorder(new TitledBorder(null, "Photo Profil", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        Photo.setBounds(550, 130, 250, 170);
        this.add(Photo);
        Photo.setLayout(null);
        l = new JLabel();
        l.setBounds(5, 16, 240, 150);



        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser file = new JFileChooser();
                file.setCurrentDirectory(new File(System.getProperty("user.home")));
                //filtrer les fichiers
                FileNameExtensionFilter filter = new FileNameExtensionFilter("*.Images",
                        ".jpg", ".png");
                file.addChoosableFileFilter(filter);
                int res = file.showSaveDialog(null);
                //si l'utilisateur clique sur enregistrer dans Jfilechooser
                if (res == JFileChooser.APPROVE_OPTION) {
                    File selFile = file.getSelectedFile();
                    String path1 = selFile.getAbsolutePath();
                    path = path1;
                    l.setIcon(resize(path));
                    Photo.add(l);
                }
            }
        });

        this.setLayout(null);
        int i = 00;
        for(JTextField field : tab_field){

            field.setBounds(160, 100+40*i, 200, 30);
            this.add(field);
            field.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.BLACK));
            field.setBackground(null);
            field.setColumns(10);
            field.setEditable(false);
            i++;
        }

        i = 0;
        for(JLabel text: texts){
            text.setBounds( 20, 100+40*i  , 120, 30);
            this.add(text);
            i++;
        }

        this.add(sauvgarder);
        this.sauvgarder.setBounds(400,600,150,30);
        this.sauvgarder.setBackground(Color.WHITE);
        this.sauvgarder.setForeground(Color.BLACK);
        this.sauvgarder.setEnabled(false);

        this.add(modifier);
        this.modifier.setBounds(200,600,150,30);
        this.modifier.setBackground(Color.WHITE);
        this.modifier.setForeground(Color.BLACK);

        try {
            ResultSet rs;
            rs = Conn.createStatement().executeQuery("select * from DONNEUR where" +
                    " NUM_DONNEUR="+Integer.parseInt(Num_donneur));
            if(rs.next()){
                textField_1.setText(rs.getString(1));
                textField_2.setText(rs.getString(2));
                textField_3.setText(rs.getString(3));
                textField_4.setText(rs.getString(4));
                textField_5.setText(rs.getString(5));
                textField_6.setText(rs.getString(6));
                textField_7.setText(rs.getString(8));
                textField_8.setText(rs.getString(11));
                type_sang.setText(rs.getString(9));
                type_rs.setText(rs.getString(10));
                remarque.setText(rs.getString(7));
                path = rs.getString(12);
                l.setIcon(resize(rs.getString(12)));
                Photo.add(l);
                //System.out.println(rs.getString(12));
            }
        }catch(Exception e) {
            e.printStackTrace();
        }

        this.add(lbl_remq);
        lbl_remq.setBounds(450,340,150,30);
        this.add(remarque);
        this.remarque.setBounds(450,370,300,100);
        this.remarque.setEditable(false);

        sauvgarder.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                String Query ="update donneur set Nom='" +textField_2.getText()+
                        "',prenom='" +textField_3.getText()+
                        "',adresse='" +textField_4.getText()+
                        "',num_tel=" +textField_5.getText()+
                        ",adresse_mail='" +textField_6.getText()+
                        "',remarque='" +remarque.getText()+
                        "',DATE_NAISSANCE='" +textField_7.getText()+
                        "',GROUPE_SANGUIN='" +type_sang.getText()+
                        "',RHESUS='" +type_rs.getText()+
                        "',GENRE='" +textField_8.getText()+
                        "',image='"+path+
                        "' where NUM_DONNEUR="+Num_donneur;
                try {
                    ResultSet rs;
                    rs = Conn.createStatement().executeQuery(Query);
                    Conn.createStatement().executeQuery("commit");
                    sauvgarder.setEnabled(false);
                    btn.setEnabled(false);
                    for(JTextField field : tab_field){
                        field.setEditable(false);
                    }
                    remarque.setEditable(false);
                }catch(Exception Z) {
                    Z.printStackTrace();
                }
            }
        });

        modifier.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                for(JTextField field : tab_field){
                    field.setEditable(true);
                }
                tab_field[0].setEditable(false);
                remarque.setEditable(true);
                sauvgarder.setEnabled(true);
                btn.setEnabled(true);
            }
        });

        setSize(hauteur, longueur);
        setVisible(false);
    }


    public ImageIcon resize(String imgPath) {
        ImageIcon path = new ImageIcon(imgPath);
        Image img = path.getImage();
        Image newImg = img.getScaledInstance(l.getWidth(), l.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon image = new ImageIcon(newImg);
        return image;
    }

}

