package com.company;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Authentification extends JFrame {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private JButton connecter = new JButton("Connecter");
    private JButton fermer = new JButton();


    private JTextField login = new JTextField();
    private JPasswordField password = new JPasswordField();

    private JLabel login_txt = new JLabel("Email         :");
    private JLabel password_txt = new JLabel("PassWord :");

    private ImageIcon image = new ImageIcon("img/fond1.jpg");
    private JLabel image1 = new JLabel(image);

    private ImageIcon quit = new ImageIcon("img/exit.png");

    public void Fclose() {
        this.dispose();
    }


    public void MessageErreur() {
        JLabel erreur = new JLabel("verifiez votre mot de passe ou le non d'utilisateur");
        erreur.setForeground(Color.RED);
        this.getContentPane().add(erreur);
        erreur.setBounds(0, 0, 200, 30);
    }
    void Femer(){
        dispose();
    }
    JLabel lblNewLabel_2 = new JLabel("<html><u>Cree un compte?</u></html>");

    public Authentification(Connection Conn){

        this.setIconImage(new ImageIcon("img/icon1.png").getImage());
        this.setSize(800, 400);
        this.setLocation(0,0);
        this.setUndecorated(true);
        this.setResizable(true);
        this.getContentPane().setLayout(null);
        this.getContentPane().setBackground(Color.WHITE);

        this.setLocationRelativeTo(null);

        this.getContentPane().add(connecter);
        this.connecter.setBounds(550, 250, 200, 30);
        this.connecter.setBackground(Color.WHITE);

        this.getContentPane().add(lblNewLabel_2);
        this.lblNewLabel_2.setBounds(600, 280, 200, 30);
        lblNewLabel_2.setForeground(Color.BLACK);

        this.getContentPane().add(fermer);
        this.fermer.setBounds(750, 0, 50, 50);
        this.fermer.setIcon(quit);
        this.fermer.setOpaque(false);
        this.fermer.setContentAreaFilled(false);
        this.fermer.setBorderPainted(false);

        this.getContentPane().add(password);
        this.password.setBounds(550, 210, 200, 30);
        //this.password.setBorder(null);

        this.getContentPane().add(login);
        this.login.setBounds(550, 170, 200, 30);



        this.getContentPane().add(login_txt);
        this.login_txt.setBounds(450, 170, 200, 30);
        this.login_txt.setForeground(Color.BLACK);

        this.getContentPane().add(password_txt);
        this.password_txt.setBounds(450, 210, 200, 30);
        this.password_txt.setForeground(Color.BLACK);

        this.getContentPane().add(image1);
        this.image1.setBounds(-150, 0, 800, 400);

        this.setVisible(true);

        fermer.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent arg0) {
                Fclose();
            }
        });

        connecter.addMouseListener(new MouseAdapter() {
            @SuppressWarnings("deprecation")
            public void mouseClicked(MouseEvent arg0) {
                String Query = "select * from UTILISATEURGL where email='"+login.getText()+"'";
                try {

                    Statement stat = Conn.createStatement();
                    ResultSet rs = stat.executeQuery(Query);

                    if(rs.next()){
                        Query =  "select * from UTILISATEURGL where email='"+login.getText()+"'" +
                                "and password='"+password.getText()+"'";
                        rs = stat.executeQuery(Query);
                        if(rs.next()){
                            if(rs.getString(3).equals("Donneur")){
                                ResultSet rs_num = stat.executeQuery("select num_donneur from donneur where " +
                                        "Adresse_mail='"+login.getText()+"'");
                                rs_num.next();
                                Femer();
                                FenetrePrincipale x = new FenetrePrincipale(rs_num.getString(1),Conn);
                            }else{
                                Femer();
                                FenetrePrincipale x = new FenetrePrincipale("-1"+rs.getString(1),Conn);
                            }
                        }else {
                            JOptionPane.showMessageDialog(null, "Mot de passe incorrect",
                                    "Erreur", JOptionPane.WARNING_MESSAGE);
                        }
                    }else{
                        JOptionPane.showMessageDialog(null, "Utilisateur existe pas ",
                                "Erreur", JOptionPane.WARNING_MESSAGE);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }


            }

        });



        lblNewLabel_2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                CreeCompte y = new CreeCompte(Conn);
            }
            @Override
            public void mouseExited(MouseEvent e) {lblNewLabel_2.setForeground(Color.BLACK);}
            @Override
            public void mouseEntered(MouseEvent e) {lblNewLabel_2.setForeground(Color.BLUE);}
        });
    }
}
