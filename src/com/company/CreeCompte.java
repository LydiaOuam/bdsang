package com.company;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.ParseException;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.MaskFormatter;

public class CreeCompte extends  JFrame{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private JLabel titre = new JLabel("<html><h1><u>Cree votre compte</u></h1></html>");
    JLabel l;
//    private MaskFormatter formatter;
//
//    {
//        try {
//            formatter = new MaskFormatter("##/##/####");
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }
    private JTextField num_donneur = new JTextField(),nom = new JTextField(),prenom = new JTextField(),
            adresse = new JTextField(),num_tel = new JTextField(),date_naissance = new JTextField("jj/mm/aaaa"),
            email = new JTextField(),password = new JPasswordField();

    private JTextField donnees[] = {num_donneur,nom,prenom,adresse,num_tel,date_naissance,email,password};
    private JLabel lbl[] = {new JLabel("Num Donneur :"),new JLabel("Nom :"),new JLabel("Prenom :"),
            new JLabel("Adresse :"),new JLabel("Numero tel :"),new JLabel("date naissance :"),
            new JLabel("Email :"),new JLabel("Password :")};

    JButton btn = new JButton("Parcourir"),btn_creation = new JButton("Creee Compte");

    String path_photo="Aucun";
    public CreeCompte(Connection Conn) {

        this.setResizable(false);
        this.setIconImage(new ImageIcon("img/icon1.png").getImage());
        setSize(800, 550);
        this.setTitle("Cree Compte");
        this.getContentPane().add(titre);
        titre.setBounds((800/2)-100,10,300,30);
        getContentPane().add(btn);
        this.setLocationRelativeTo(null);
        int i = 1;
        for(JLabel x:lbl){
            x.setBounds(20, 40*(i+1), 100, 30);
            getContentPane().add(x);
            i++;
        }
        i=1;
        for(JTextField x:donnees){
            x.setBounds(120, 40*(i+1), 150, 30);
            x.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.BLACK));
            x.setBackground(null);
            x.setColumns(10);
            getContentPane().add(x);
            i++;
        }
        JPanel Photo = new JPanel();
        Photo.setBorder(new TitledBorder(null, "Photo profil", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        Photo.setBounds(418, 80, 196, 163);
        getContentPane().add(Photo);
        Photo.setLayout(null);

        l = new JLabel();
        l.setBounds(6, 16, 184, 141);
        Photo.add(l);

        this.getContentPane().add(btn_creation);
        btn_creation.setBounds(300,450,150,30);
        btn_creation.setBackground(Color.WHITE);
        btn_creation.setForeground(Color.BLACK);
        btn.setBounds(472, 241, 100, 29);
        btn.setBackground(Color.WHITE);
        btn.setForeground(Color.BLACK);
        getContentPane().setLayout(null);

        JComboBox comboBox = new JComboBox();
        comboBox.setModel(new DefaultComboBoxModel(new String[]{"A", "B", "AB", "O"}));
        comboBox.setBounds(500,40*(i-2), 150, 30);
        getContentPane().add(comboBox);
        JLabel lbl_1 = new JLabel("Type sang :");
        lbl_1.setBounds(400, 40*(i-2), 100, 30);
        getContentPane().add(lbl_1);
        i++;
        JComboBox comboBox_1 = new JComboBox();
        comboBox_1.setModel(new DefaultComboBoxModel(new String[]{"+", "-"}));
        comboBox_1.setBounds(500, 40*(i-2), 150, 30);
        getContentPane().add(comboBox_1);
        JLabel lbl_2 = new JLabel("Type rhesus :");
        lbl_2.setBounds(400, 40*(i-2), 100, 30);
        getContentPane().add(lbl_2);
        i++;
        JComboBox comboBox_2 = new JComboBox();
        comboBox_2.setModel(new DefaultComboBoxModel(new String[]{"Homme", "Femme"}));
        comboBox_2.setBounds(500, 40*(i-2), 150, 30);
        getContentPane().add(comboBox_2);
        JLabel lbl_4 = new JLabel("genre :");
        lbl_4.setBounds(400, 40*(i-2), 100, 30);
        getContentPane().add(lbl_4);
        i++;
        JEditorPane editorPane = new JEditorPane();
        editorPane.setBounds(500, 40*(i-2), 200, 100);
        getContentPane().add(editorPane);
        JLabel lbl_3 = new JLabel("Remarque :");
        lbl_3.setBounds(400, 40*(i-2), 100, 30);
        getContentPane().add(lbl_3);



        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser file = new JFileChooser();
                file.setCurrentDirectory(new File(System.getProperty("user.home")));
                //filtrer les fichiers
                FileNameExtensionFilter filter = new FileNameExtensionFilter("*.Images", "jpg", "png");
                file.addChoosableFileFilter(filter);
                int res = file.showSaveDialog(null);
                //si l'utilisateur clique sur enregistrer dans Jfilechooser
                if (res == JFileChooser.APPROVE_OPTION) {
                    File selFile = file.getSelectedFile();
                    String path = selFile.getAbsolutePath();
                    path_photo = path;
                    l.setIcon(resize(path));
                }
            }
        });

        btn_creation.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                String Query = "insert into Donneur values("+
                        num_donneur.getText()+",'"+
                        nom.getText()+"','"+
                        prenom.getText()+"','"+
                        adresse.getText()+"',"+
                        num_tel.getText()+",'"+
                        email.getText()+"','"+
                        editorPane.getText()+"','"+
                        date_naissance.getText()+"','"+
                        (String)comboBox.getSelectedItem()+"','"+
                        (String)comboBox_1.getSelectedItem()+"','"+
                        (String)comboBox_2.getSelectedItem()+"','"+
                        path_photo+"')";

                int rep = JOptionPane.showConfirmDialog(
                        null,
                        "Cree compte?",
                        "Confirmation",
                        JOptionPane.INFORMATION_MESSAGE);
                if(rep==0){
                    try {
                        ResultSet rs;
                        rs = Conn.createStatement().executeQuery("select * from UTILISATEURGL where " +
                                "email='"+email.getText()+"'");
                        if(rs.next()){
                            JOptionPane.showMessageDialog(null, "Compte existe deja",
                                    "Erreur", JOptionPane.WARNING_MESSAGE);
                        }else{
                            rs = Conn.createStatement().executeQuery("select * from donneur where " +
                                    "num_donneur='"+ num_donneur.getText()+"'");
                            if(rs.next()){
                                JOptionPane.showMessageDialog(null, "Donneur existe deja " +
                                                "un compte sera cree",
                                        "Information", JOptionPane.INFORMATION_MESSAGE);
                                rs = Conn.createStatement().executeQuery("insert into UTILISATEURGL values('"+
                                        rs.getString(6)+"','"+
                                        password.getText()+"','Donneur')");
                                Conn.createStatement().executeQuery("Commit");
                            }else{
                                rs = Conn.createStatement().executeQuery(Query);
                                rs = Conn.createStatement().executeQuery("insert into UTILISATEURGL values('"+
                                        email.getText()+"','"+
                                        password.getText()+"','Donneur')");
                                Conn.createStatement().executeQuery("Commit");
                            }

                            JOptionPane.showMessageDialog(null, "Compte cree",
                                    "Information", JOptionPane.INFORMATION_MESSAGE);
                        }
                    }catch(Exception y) {
                        y.printStackTrace();
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Creation annulee",
                            "Information", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
        setVisible(true);
    }

    // M�thode pour redimensionner l'image avec la m�me taille du Jlabel
    public ImageIcon resize(String imgPath) {
        ImageIcon path = new ImageIcon(imgPath);
        Image img = path.getImage();
        Image newImg = img.getScaledInstance(l.getWidth(), l.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon image = new ImageIcon(newImg);
        return image;
    }

}

