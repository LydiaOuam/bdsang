package com.company;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import net.proteanit.sql.DbUtils;

public class DemandeRV extends JPanel {
    public JTable rv = new JTable();
    public JScrollPane rdvPan = new JScrollPane(rv);
    private JButton accepter =new JButton("Accepter"),
            refuser = new JButton("Refuser"),
            actu = new JButton("Actualiser");

    private JLabel[] lbl= {new JLabel("Num donneur :"),new JLabel("Nom :"),
            new JLabel("Prenom :"),new JLabel("Date naissance"),new JLabel("Etat sante"),
            new JLabel("Date rendez-vous :"),};

    private JLabel nom = new JLabel(),prenom=new JLabel(),
            num_donneur = new JLabel(),date_rv=new JLabel(),etat_sante = new JLabel(),date_nais = new JLabel(),
            titre = new JLabel("<html><u>Liste demande rendez-vous</u></html>");

    private JLabel[] champs = {num_donneur,nom,prenom,date_nais,etat_sante,date_rv};

    private void Affichage(Connection Conn){

        try {
            ResultSet rs;
            rs = Conn.createStatement().executeQuery("select * from Demande_RV Where etat='Attente'");
            rv.setModel(DbUtils.resultSetToTableModel(rs));

        } catch (Exception z) {
            JOptionPane.showMessageDialog(null, "Alerter les administrateur",
                    "Erreur 404", JOptionPane.WARNING_MESSAGE);
        }
    }

    public DemandeRV(int x, int y, int longueur, int hauteur, Connection Conn) {
        setSize( longueur,hauteur);
        this.setLayout(null) ;
        this.add(titre);
        this.titre.setBounds((longueur/2) -300,10,600,90);
        this.titre.setFont(new java.awt.Font("Monospaced", 1, 40));

        this.add(actu);
        this.actu.setBounds(750,120,150,30);
        actu.setBackground(Color.WHITE);
        actu.setForeground(Color.BLACK);
        this.add(rdvPan);
        this.rdvPan.setBounds(500,150,400,300);
        this.rdvPan.setBackground(Color.GRAY);
        rv.setEditingColumn(1);
        this.Affichage(Conn);

        int i = 0;
        for(JLabel l : lbl){
            this.add(l);
            l.setBounds(50, 150+(40*i), 150, 30);
            i++;
        }
        i=0;
        for(JLabel l : champs){
            this.add(l);
            l.setBounds(180, 150+(40*i), 200, 30);
            l.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.BLACK));
            l.setBackground(null);
            i++;
        }
        this.add(accepter);
        accepter.setBackground(Color.WHITE);
        accepter.setForeground(Color.BLACK);
        this.add(refuser);
        refuser.setBackground(Color.WHITE);
        refuser.setForeground(Color.BLACK);
        this.accepter.setBounds(240,250+(40*i),150,30);
        this.refuser.setBounds(450,250+(40*i),150,30);

        actu.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Affichage(Conn);
            }
        });

        accepter.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    if(num_donneur.getText().length() != 0){
                        ResultSet rs;
                        rs = Conn.createStatement().executeQuery("update Demande_RV set etat='Acceptee' where " +
                                "NUM_DONNEUR="+num_donneur.getText()+" " +
                                "and DATE_RV='"+date_rv.getText().substring(0,10)+"' " +
                                "and Heure='"+date_rv.getText().substring(13,18)+"'");
                        Affichage(Conn);

                    }else{
                        JOptionPane.showMessageDialog(null, "Selectionnez un rendez vous",
                                "Attention", JOptionPane.ERROR_MESSAGE);
                    }


                } catch (Exception z) {
                    JOptionPane.showMessageDialog(null, "Alertez les administrateur",
                            "Erreur 404", JOptionPane.WARNING_MESSAGE);
                }
            }
        });

        refuser.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    if(num_donneur.getText().length() != 0){

                        ResultSet rs;
                        rs = Conn.createStatement().executeQuery("update Demande_RV set etat='Refusee' where " +
                                "NUM_DONNEUR="+num_donneur.getText()+" " +
                                "and DATE_RV='"+date_rv.getText().substring(0,10)+"' " +
                                "and Heure='"+date_rv.getText().substring(13,18)+"'");

                        Affichage(Conn);
                    }else{

                        JOptionPane.showMessageDialog(null, "Selectionnez un rendez vous",
                                "Attention", JOptionPane.ERROR_MESSAGE);
                    }

                } catch (Exception z) {
                    JOptionPane.showMessageDialog(null, "Alertez les administrateur",
                            "Erreur 404", JOptionPane.WARNING_MESSAGE);
                    //z.printStackTrace();
                }
            }
        });

        rv.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    int i = rv.getSelectedRow();
                    //num_donneur,nom,prenom,date_nais,etat_sante,date_rv
                    num_donneur.setText(""+rv.getValueAt(i,0));
                    date_rv.setText(""+rv.getValueAt(i,1)+" a "+rv.getValueAt(i,2));
                    //System.out.println(rv.getValueAt(i,0));

                    ResultSet rs;
                    rs = Conn.createStatement().executeQuery("select * from donneur where " +
                            "NUM_DONNEUR="+num_donneur.getText());
                    if(rs.next()){
                        nom.setText(rs.getString(2));
                        prenom.setText(rs.getString(3));
                        date_nais.setText(rs.getString(8));
                        etat_sante.setText(rs.getString(7));
                    }

                }
                catch (Exception z) {

                    JOptionPane.showMessageDialog(null, "Alertez les administrateur",
                            "Erreur 404", JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        this.setVisible(false);
    }
}
