package com.company;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Contacter extends JPanel {

    Border LoweredBevelBorder = BorderFactory.createLoweredBevelBorder();
    Border EtchedBorderRaised = BorderFactory.createTitledBorder(LoweredBevelBorder, "Notification",
            TitledBorder.LEFT,TitledBorder.TOP,
            new Font("Arial", Font.PLAIN , 13), Color.black);
    Border bordertitre = BorderFactory.createTitledBorder("Notification");
    Border bordertitre1 = BorderFactory.createTitledBorder("Alertes");
    Border bordertitre2 = BorderFactory.createTitledBorder("Message");

    private JEditorPane eddit_msg = new JEditorPane(),notif = new JEditorPane(),alerte = new JEditorPane();
    private JTextField distinataire = new JTextField();
    private JScrollPane zone_msg = new JScrollPane(eddit_msg);

    private JLabel txt_dist = new JLabel("Distinataire (numero donneur):"),
                    txt_msg = new JLabel("Message"),
                    titre   = new JLabel("Messagerie"),
                    msg1 = new JLabel("**notification sera envoyee a tous les utilisateurs**"),
                    msg2 = new JLabel("**alerte sera envoyee a tous les utilisateurs**");;

    private JButton envoyer = new JButton("Envoyer"),
                    rechercher = new JButton("Rechercher"),
                    envoyerNotif = new JButton("Envoyer Notification"),
                    envoyerAlerte = new JButton("Envoyer Alerte");



    private JPanel notifications = new JPanel(), alertes = new JPanel(),msg = new JPanel();

    public Contacter(int x, int y, int longueur, int hauteur, Connection Conn,String Num_admin) {
        setSize( longueur,hauteur);
        this.setLayout(null) ;
        this.add(titre);
        this.titre.setBounds((longueur/2) -150,10,600,90);
        this.titre.setFont(new java.awt.Font("Monospaced", 1, 40));
        this.add(txt_dist);
        this.txt_dist.setBounds(50,90,300,30);

        this.add(distinataire);
        this.distinataire.setBounds(50,120,400,30);

        this.add(txt_msg);
        this.txt_msg.setBounds(50,150,100,30);

        this.add(zone_msg);
        this.zone_msg.setBounds(50,180, 400, 300);

        this.add(envoyer);
        this.envoyer.setBounds(300,490,150,30);
        this.envoyer.setBackground(Color.WHITE);
        this.envoyer.setForeground(Color.BLACK);

        this.add(notifications);
        this.notifications.setLayout(null);
        this.notifications.setBounds(500,100,400,230);
        this.notifications.setBorder(bordertitre);
        this.notifications.add(envoyerNotif);

        this.envoyerNotif.setBounds(125,180,150,30);
        this.envoyerNotif.setBackground(Color.WHITE);
        this.envoyerNotif.setForeground(Color.BLACK);
        this.envoyerNotif.setBorder(null);

        this.notifications.add(notif);
        this.notif.setBounds(10,20,380,130);
        this.notifications.add(msg1);
        this.msg1.setBounds(10,140,300,30);

        this.add(alertes);
        this.alertes.setLayout(null);
        this.alertes.setBounds(500,380,400,230);
        this.alertes.setBorder(bordertitre1);
        this.alertes.add(envoyerAlerte);

        this.envoyerAlerte.setBounds(125,180,150,30);
        this.envoyerAlerte.setBackground(Color.WHITE);
        this.envoyerAlerte.setForeground(Color.BLACK);
        this.envoyerAlerte.setBorder(null);

        this.alertes.add(alerte);
        this.alerte.setBounds(10,20,380,130);
        this.alertes.add(msg2);
        this.msg2.setBounds(10,140,300,30);


        this.add(msg);
        this.msg.setBorder(bordertitre2);
        this.msg.setBounds(20,100,450,510);
        //this.msg.setBackground(Color.black);
        this.msg.setLayout(null);

        this.msg.add(txt_dist);
        this.txt_dist.setBounds(20,20,300,30);

        this.msg.add(distinataire);
        this.distinataire.setBounds(20,50,400,30);

        this.msg.add(txt_msg);
        this.txt_msg.setBounds(20,80,100,30);

        this.msg.add(zone_msg);
        this.zone_msg.setBounds(20,110, 400, 300);

        this.msg.add(envoyer);
        this.envoyer.setBounds(270,420,150,30);
        this.envoyer.setBackground(Color.WHITE);
        this.envoyer.setForeground(Color.BLACK);
        this.envoyer.setBorder(null);

        envoyer.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(eddit_msg.getText().length()<=500){
                    String Query = "select * from Donneur where NUM_DONNEUR="+distinataire.getText()+"";
                    try {
                        ResultSet rs;
                        rs = Conn.createStatement().executeQuery(Query);
                        if(rs.next()){
                            int rep = JOptionPane.showConfirmDialog(
                                    null,
                                    "Confirmez l'envoi",
                                    "Confirmation",
                                    JOptionPane.INFORMATION_MESSAGE);
                            if(rep==0){
                                DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                Calendar calendar = Calendar.getInstance();
                                Query = "insert into message values ('"+Num_admin+"','"+
                                        format.format(calendar.getTime())+"','"+
                                        eddit_msg.getText()+"','"+
                                        distinataire.getText()+"')";

                                rs = Conn.createStatement().executeQuery(Query);

                                Conn.createStatement().executeQuery("Commit");
                                JOptionPane.showMessageDialog(
                                        null,
                                        "Message envoyer",
                                        "Information",
                                        JOptionPane.INFORMATION_MESSAGE
                                );
                            }else{
                                JOptionPane.showMessageDialog(
                                        null,
                                        "Envoi annulee",
                                        "Information",
                                        JOptionPane.INFORMATION_MESSAGE
                                );
                            }


                        }else{
                            JOptionPane.showMessageDialog(null, "Donneur n'existe pas",
                                    "Attention", JOptionPane.WARNING_MESSAGE);
                        }
                    }catch(Exception y) {
                        y.printStackTrace();
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Message trop long " +
                                    ""+eddit_msg.getText().length()+"/500",
                            "Attention", JOptionPane.WARNING_MESSAGE);
                }

            }
        });
        envoyerAlerte.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String Query;
             
                try {


                    int rep = JOptionPane.showConfirmDialog(
                            null,
                            "Confirmez l'envoi",
                            "Confirmation",
                            JOptionPane.INFORMATION_MESSAGE);
                    if(rep==0){
                        DateFormat format = new SimpleDateFormat("yyyy-MM-dd  hh:mm:ss");
                        Calendar calendar = Calendar.getInstance();
                        Query = "insert into Alerte values ('"+Num_admin+"','" +
                                format.format(calendar.getTime()) + "','" +
                                alerte.getText() + "')";

                        ResultSet rs = Conn.createStatement().executeQuery(Query);
                        Conn.createStatement().executeQuery("Commit");
                        JOptionPane.showMessageDialog(
                                null,
                                "Notification envoyer",
                                "Information",
                                JOptionPane.INFORMATION_MESSAGE
                        );
                    }else{
                        JOptionPane.showMessageDialog(
                                null,
                                "Envoi annulee",
                                "Information",
                                JOptionPane.INFORMATION_MESSAGE
                        );
                    }
                } catch (Exception y) {
                    JOptionPane.showMessageDialog(null, "Erreur alerte non envoyee",
                            "Attention", JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        envoyerNotif.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                String Query;
                try {

                    int rep = JOptionPane.showConfirmDialog(
                            null,
                            "Confirmez l'envoi",
                            "Confirmation",
                            JOptionPane.INFORMATION_MESSAGE);
                    if(rep==0){
                        DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Calendar calendar = Calendar.getInstance();

                        Query = "insert into notification values ('"+Num_admin+"','" +
                                format.format(calendar.getTime())+ "','" +
                                notif.getText()+"')";
                        ResultSet rs = Conn.createStatement().executeQuery(Query);
                        Conn.createStatement().executeQuery("Commit");
                        JOptionPane.showMessageDialog(
                                null,
                                "Notification envoyer",
                                "Information",
                                JOptionPane.INFORMATION_MESSAGE
                        );
                    }else{
                        JOptionPane.showMessageDialog(
                                null,
                                "Envoi annulee",
                                "Information",
                                JOptionPane.INFORMATION_MESSAGE
                        );
                    }

                } catch (Exception y) {
                    JOptionPane.showMessageDialog(null, "Erreur notification non envoyee",
                            "Attention", JOptionPane.WARNING_MESSAGE);
                }
            }
        });
    }
}
