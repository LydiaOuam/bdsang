package com.company;

import net.proteanit.sql.DbUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class RendezVous extends JPanel {



    private final Object Calendrier;
    private DateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private JLabel titre = new JLabel("<html><u>Prendre rendez-Vous</u></html>");
    private JPanel calendarPan = new JPanel();
    private JLabel lbl_1 = new JLabel("Date   :"),
                   lbl_2 = new JLabel("Heure :");

    private JComboBox heures = new JComboBox(),
                      minutes = new JComboBox();
    public static AbstractButton btn = new JButton("Reserver");

    private JLabel msg = new JLabel("<html><strong>Notice :</strong> selectionnez une date depuis " +
            "le calenderie puis choisissez une heure.</html>");

    static String date;
    static JLabel lbl_date = new JLabel();


    public RendezVous(int x, int y, int longueur, int hauteur, Connection Conn, String num_donneur) {
        btn.setEnabled(false);
        this.add(titre);
        this.titre.setBounds((longueur/2) -250,50,600,90);
            this.titre.setFont(new java.awt.Font("Monospaced", 1, 40));
        Calendar calendar = Calendar.getInstance();
        String year = format.format(calendar.getTime()).substring(0,4);
        String month = format.format(calendar.getTime()).substring(6,7);
        setSize(longueur, hauteur);
        this.setLayout(null);
        this.add(calendarPan);
        this.calendarPan.setBounds(10,150,500,450);
        //this.calendarPan.setBackground(Color.black);
        Calendrier = new Calendrier(50,50,Integer.parseInt(month),Integer.parseInt(year),this.calendarPan);

        this.add(lbl_1);
        this.lbl_1.setBounds(600,300,40,30);
        this.add(lbl_date);
        lbl_date.setBounds(650,300,150,30);
        lbl_date.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.BLACK));

        this.add(lbl_2);
        this.lbl_2.setBounds(600,350,40,30);

        this.add(btn);
        btn.setBounds(600,400,220,30);
        btn.setBackground(Color.WHITE);
        btn.setForeground(Color.BLACK);
        btn.setBorder(null);


        //filtre_avec_sang.setModel(new DefaultComboBoxModel(new String[]{"aucun","A", "B", "AB", "O"}));
        this.add(heures);
        this.heures.setBounds(650,350,50,30);
        heures.setModel(new DefaultComboBoxModel(new String[]{"07h","08h","09h","10h","11h",
                "12h","13h","14h","15h","16h"}));

        this.add(minutes);
        this.minutes.setBounds(700,350,50,30);
        minutes.setModel(new DefaultComboBoxModel(new String[]{"00","10","20","30","40","50"}));

        this.add(msg);
        this.msg.setBounds(50,550,500,30);

        btn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String Query ="Select * from DEMANDE_RV where NUM_DONNEUR="+num_donneur+
                            "and DATE_RV='"+lbl_date.getText()+"' and " +
                            "HEURE='"+(String)heures.getSelectedItem()+""+(String)minutes.getSelectedItem()+"'";

                try {
                    ResultSet rs;
                    rs = Conn.createStatement().executeQuery(Query);
                    if(rs.next()){

                        JOptionPane.showMessageDialog(null, "Vous avez deja pris un" +
                                        " rendez vous pour cete date ",
                                "Attention", JOptionPane.WARNING_MESSAGE);
                    }else{
                        Query = "insert into DEMANDE_RV values("+num_donneur+",'"+
                                lbl_date.getText()+"','"+
                                (String)heures.getSelectedItem()+""+(String)minutes.getSelectedItem()+"','Attente')";
                        rs = Conn.createStatement().executeQuery(Query);
                        Conn.createStatement().executeQuery("Commit");
                        JOptionPane.showMessageDialog(null, "Rendez Vous reserver",
                                "Information", JOptionPane.INFORMATION_MESSAGE);
                    }

                }catch(Exception y) {

                    JOptionPane.showMessageDialog(null, "Erreur Contacter les administrateurs",
                            "Attention", JOptionPane.WARNING_MESSAGE);

                }
            }
        });
    }
}
