package com.company;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Forum extends JPanel {

    private JPanel listemsg = new JPanel();

    private JScrollPane panneau_msg = new JScrollPane(listemsg,ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
            ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

    private JTextArea zone_msg = new JTextArea();
    private JScrollPane pan_zon_msg = new JScrollPane(zone_msg,ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
            ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

    private JButton envoyer = new JButton(new ImageIcon("img/send.png"));
    int i;int TailLigne = 0;

    int nb = 0,pos_prochaine=0,size_precedent;
    Forum(int x, int y, int longueur, int hauteur, String Num_donneur, Connection Conn) {
        setSize( longueur,hauteur);
        this.setLayout(null) ;
        //this.setBackground(Color.BLUE);
        this.add(panneau_msg);
        this.panneau_msg.setBounds(0,0,longueur,hauteur-150);

        this.listemsg.setLayout((null));
        this.add(pan_zon_msg);
        this.pan_zon_msg.setBounds(30,hauteur-100,longueur-200,30);

        listemsg.setPreferredSize(new Dimension(panneau_msg.getWidth(), 50*i));
        this.add(envoyer);
        this.envoyer.setBounds(longueur-160,hauteur-100,100,30);
        this.envoyer.setBackground(Color.WHITE);
        this.envoyer.setForeground(Color.BLACK);
        this.zone_msg.setLineWrap(true); // pour retour a  la ligne automatique
        this.zone_msg.setWrapStyleWord(true);// pour ne pas couper les mots

        AfficherMsgs(Conn,Num_donneur,longueur);

        envoyer.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                Calendar calendar = Calendar.getInstance();
                if(zone_msg.getText().length()!=0){
                    String Query = "insert into Forum values('"+zone_msg.getText()+"',"+Num_donneur+"," +
                            "'"+format.format(calendar.getTime())+"')";
                    try {
                        ResultSet rs = Conn.createStatement().executeQuery(Query);
                        Conn.createStatement().executeQuery("Commit");
                        AfficherMsgs(Conn,Num_donneur,longueur);
                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null, "Message non envoyer",
                                "Attention", JOptionPane.WARNING_MESSAGE);
                        ex.printStackTrace();
                    }
                    zone_msg.setText(null);
                }
            }
        });
    }

    void AfficherMsgs(Connection Conn,String Num_donneur,int longueur){
        ResultSet rs;
        listemsg.removeAll();
        pos_prochaine = 0;
        try {
            String Query ="select * from forum";
            rs = Conn.createStatement().executeQuery(Query);
            while(rs.next()){

                for (int i=0; i <rs.getString(1).length(); i++) {if (rs.getString(1).charAt(i) == '\n')nb++;}
                JPanel PMsg = new JPanel(),info = new JPanel(),
                        msg = new JPanel(new FlowLayout(FlowLayout.RIGHT));
                listemsg.add(PMsg);

                ResultSet rx = Conn.createStatement().executeQuery("select * from donneur where " +
                        "num_donneur="+rs.getString(2));
                rx.next();
                JLabel infos = new JLabel();
                if(rs.getString(2).equals(Num_donneur)){
                    PMsg.setLayout(new FlowLayout(FlowLayout.RIGHT));
                    msg.setLayout(new FlowLayout(FlowLayout.RIGHT));
                    info.setLayout(new FlowLayout(FlowLayout.RIGHT));
                    infos = new JLabel("Vous a "+rs.getString(3));
                }else{
                    PMsg.setLayout(new FlowLayout(FlowLayout.LEFT));
                    msg.setLayout(new FlowLayout(FlowLayout.LEFT));
                    info.setLayout(new FlowLayout(FlowLayout.LEFT));
                    infos = new JLabel(rx.getString(2)+" "+rx.getString(3)+" a" +
                            " "+rs.getString(3));
                }
                JTextArea text = new JTextArea(rs.getString(1));
                text.setEditable(false);
                PMsg.setLayout(null);
                PMsg.add(info);
                PMsg.add(msg);
                msg.add(text);

                info.add(infos);

                PMsg.setBounds(0,pos_prochaine,longueur-25,40+(20*nb));
                info.setBounds(rs.getString(2).equals(Num_donneur)?longueur-320:0,0,300,20);
                msg.setBounds(0,20,longueur-25,(20*text.getLineCount()));

                pos_prochaine = pos_prochaine +(50+(20*nb));



                nb=0;
            }
            listemsg.setPreferredSize(new Dimension(panneau_msg.getWidth(),pos_prochaine ));
            listemsg.revalidate();
            listemsg.repaint();
        } catch (Exception r) {
            JOptionPane.showMessageDialog(null, "Erreur ",
                    "Attention", JOptionPane.WARNING_MESSAGE);
        }
    }



}
/*Cote generale
/*
        ResultSet rs;

        try {
            String Query ="select * from forum";
            rs = Conn.createStatement().executeQuery(Query);
            while(rs.next()){

                for (int i=0; i <rs.getString(1).length(); i++) {if (rs.getString(1).charAt(i) == '\n')nb++;}
                JPanel PMsg = new JPanel(),info = new JPanel(),
                        msg = new JPanel(new FlowLayout(FlowLayout.RIGHT));
                listemsg.add(PMsg);

                ResultSet rx = Conn.createStatement().executeQuery("select * from donneur where " +
                        "num_donneur="+rs.getString(2));
                rx.next();
                JLabel infos = new JLabel();
                if(rs.getString(2).equals(Num_donneur)){
                    PMsg.setLayout(new FlowLayout(FlowLayout.RIGHT));
                    msg.setLayout(new FlowLayout(FlowLayout.RIGHT));
                    info.setLayout(new FlowLayout(FlowLayout.RIGHT));
                    infos = new JLabel("Vous Le "+rs.getString(3));
                }else{
                    PMsg.setLayout(new FlowLayout(FlowLayout.LEFT));
                    msg.setLayout(new FlowLayout(FlowLayout.LEFT));
                    info.setLayout(new FlowLayout(FlowLayout.LEFT));
                     infos = new JLabel(rx.getString(2)+" "+rx.getString(3)+" a" +
                            " "+rs.getString(3));
                }
                JTextArea text = new JTextArea(rs.getString(1));
                text.setEditable(false);
                PMsg.setLayout(null);
                PMsg.add(info);
                PMsg.add(msg);
                msg.add(text);

                info.add(infos);

                PMsg.setBounds(0,pos_prochaine,longueur-25,40+(20*nb));
                info.setBounds(rs.getString(2).equals(Num_donneur)?longueur-300:0,0,300,20);
                msg.setBounds(0,20,longueur-25,(20*text.getLineCount()));

                pos_prochaine = pos_prochaine +(50+(20*nb));
                listemsg.setPreferredSize(new Dimension(panneau_msg.getWidth(), pos_prochaine));
                listemsg.revalidate();
                listemsg.repaint();

                nb=0;
            }
            listemsg.setPreferredSize(new Dimension(panneau_msg.getWidth(),pos_prochaine ));

        } catch (Exception r) {
            JOptionPane.showMessageDialog(null, "Erreur ",
                    "Attention", JOptionPane.WARNING_MESSAGE);
        }*/


/*cote bouton
 /*
                JPanel PMsg = new JPanel(),info = new JPanel(new FlowLayout(FlowLayout.RIGHT)),
                        msg = new JPanel(new FlowLayout(FlowLayout.RIGHT));
                listemsg.add(PMsg);
                JTextArea text = new JTextArea(zone_msg.getText());
                PMsg.setLayout(null);
                PMsg.add(info);
                PMsg.add(msg);
                msg.add(text);
                info.add(new JLabel("Vous Le "+format.format(calendar.getTime())));
                //PMsg.setBackground(Color.YELLOW);
                PMsg.setBounds(0,pos_prochaine,longueur-20,40+(20*text.getLineCount()));
                info.setBounds(longueur-300,0,300,20);
                msg.setBounds(0,20,longueur-20,(20*text.getLineCount()));
                pos_prochaine = pos_prochaine +(50+(20*text.getLineCount()));
                listemsg.setPreferredSize(new Dimension(panneau_msg.getWidth(), pos_prochaine));
                listemsg.revalidate();
                listemsg.repaint();*/
