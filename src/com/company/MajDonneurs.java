package com.company;


import net.proteanit.sql.DbUtils;

import javax.swing.*;
import javax.swing.text.MaskFormatter;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.ParseException;

public class MajDonneurs extends JPanel {

    private JComboBox filtre_avec_sang = new JComboBox(),filtre_avec_rh = new JComboBox(),
            liste_rh = new JComboBox(),liste_sang = new JComboBox(),liste_genre = new JComboBox();


    private JButton annuler = new JButton("Annuler"),ajouter =new JButton("Ajouter"),
            modifier = new JButton("Modifier"),supprimer =new JButton("Supprimer"),
            rechercher = new JButton("Rechercher");
//    private MaskFormatter formatter;
//
//    {
//        try {
//            formatter = new MaskFormatter("##/##/####");
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }

    private JTextField textField_1= new JTextField(),textField_2= new JTextField(),textField_3= new JTextField(),
                       textField_4= new JTextField(),textField_5= new JTextField(),textField_6= new JTextField(),
                       textField_7= new JTextField(),textField_8= new JTextField("jj/mm/aaaa"),
                       recherche_donneur = new JTextField();

    private JTextField[] tab_field =  {textField_1,textField_2,textField_3,textField_4,textField_5,textField_6,
            textField_7,textField_8};

    private JLabel[] texts = {new JLabel("Code Donneur"),new JLabel("Nom"),new JLabel("Prenom"),
            new JLabel("Adresse"),
            new JLabel("Numero telephone:"),new JLabel("Adresse mail:"),
            new JLabel("Remarques:"),new JLabel("Date de naissance"),
            new JLabel("Groupe sang"),new JLabel("Rhesus")};

    public MajDonneurs(int x, int y, int hauteur, int longueur, Connection Conn){
        setSize(longueur, hauteur);
        this.setLayout(null);

        this.add(rechercher);
        this.rechercher.setBackground(Color.WHITE);
        this.rechercher.setForeground(Color.BLACK);
        this.rechercher.setBounds(505,30,150,30);
        this.rechercher.setBorder(null);

        this.add(recherche_donneur);
        this.recherche_donneur.setBounds(300,30,200,30);
        this.recherche_donneur.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.BLACK));
        this.recherche_donneur.setBackground(null);
        this.recherche_donneur.setText("Search");

        /*
        JLabel lblNewLabel = new JLabel("<html><u>Gestion Donneur</u></html>");
        lblNewLabel.setBounds(218, 30, 113, 14);
        this.add(lblNewLabel);*/

        int i = 0,j=160;
        for(JTextField field : tab_field){
            if(i == texts.length/2) {
                j = 650;
                i = 0;
            }
            field.setBounds(j, 100+40*i, 150, 30);
            this.add(field);
            i++;
        }
        this.add(liste_sang);
        this.liste_sang.setBounds(j,100+40*i,150,30);
        liste_sang.setModel(new DefaultComboBoxModel(new String[]{"aucun","A", "B", "AB", "O"}));
        i++;
        this.add(liste_rh);
        this.liste_rh.setBounds(j,100+40*i,150,30);
        liste_rh.setModel(new DefaultComboBoxModel(new String[]{"aucun","+","-"}));
        i++;
        this.add(liste_genre);
        this.liste_genre.setBounds(j,100+40*i,150,30);
        liste_genre.setModel(new DefaultComboBoxModel(new String[]{"aucun","Feminin","Homme"}));
        i = 0;
        j=20;
        int k = 0;
        for(JLabel text: texts){
            if(i == texts.length/2){
                j= 500;i=0;
            }
            text.setBounds( j, 100+40*i  , 120, 30);
            this.add(text);
            i++;
        }
        JLabel genre = new JLabel("Genre ");
        genre.setBounds( j, 100+40*i  , 120, 30);
        this.add(genre);
        i++;
        this.add(ajouter);
        this.ajouter.setBounds(50,100+50*i,150,30);
        this.ajouter.setBackground(Color.WHITE);
        this.ajouter.setForeground(Color.BLACK);
        this.add(modifier);
        this.modifier.setBounds(250,100+50*i ,150,30);
        this.modifier.setBackground(Color.WHITE);
        this.modifier.setForeground(Color.BLACK);
        this.add(supprimer);
        this.supprimer.setBounds(450,100+50*i ,150,30);
        this.supprimer.setBackground(Color.WHITE);
        this.supprimer.setForeground(Color.BLACK);
        this.add(annuler);
        this.annuler.setBounds(650,100+50*i ,150,30);
        this.annuler.setBackground(Color.WHITE);
        this.annuler.setForeground(Color.BLACK);

        ajouter.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String Query = "insert into Donneur values("+
                        textField_1.getText()+",'"+
                        textField_2.getText()+"','"+
                        textField_3.getText()+"','"+
                        textField_4.getText()+"',"+
                        textField_5.getText()+",'"+
                        textField_6.getText()+"','"+
                        textField_7.getText()+"','"+
                        textField_8.getText()+"','"+
                        (String)liste_sang.getSelectedItem()+"','"+
                        (String)liste_rh.getSelectedItem()+"','"+
                        (String)liste_genre.getSelectedItem()+"','Indisponible')";

                try {
                    int rep = JOptionPane.showConfirmDialog(
                            null,
                            "Voulez-vous Ajouter?",
                            "Confirmation",
                            JOptionPane.INFORMATION_MESSAGE);
                    if(rep==0){
                        ResultSet rs;
                        rs = Conn.createStatement().executeQuery(Query);
                        Conn.createStatement().executeQuery("Commit");
                        JOptionPane.showMessageDialog(null,
                                "Donneur N�"+textField_1.getText()+" sura ajoute",
                                "Infromation", JOptionPane.INFORMATION_MESSAGE);
                    }else{
                        JOptionPane.showMessageDialog(
                                null,
                                "Ajout annule",
                                "Information",
                                JOptionPane.INFORMATION_MESSAGE
                        );
                    }

                }catch(Exception y) {
                    y.printStackTrace();
                }
            }
        });

        modifier.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                String Query= "Update Donneur set   NOM='"+ textField_2.getText()+
                                                    "' ,PRENOM='"+ textField_3.getText()+
                                                    "' ,ADRESSE='"+ textField_4.getText()+
                                                    "' ,NUM_TEL="+ textField_5.getText()+
                                                    "  ,ADRESSE_MAIL='"+ textField_6.getText()+
                                                    "' ,REMARQUE='"+ textField_7.getText()+
                                                    "' ,DATE_NAISSANCE='"+ textField_8.getText()+
                                                    "' ,GROUPE_SANGUIN='"+ (String)liste_sang.getSelectedItem()+
                                                    "' ,RHESUS='"+ (String)liste_rh.getSelectedItem()+
                                                    "' ,GENRE='"+(String) liste_genre.getSelectedItem()+
                                                    "' where NUM_DONNEUR="+textField_1.getText()+"";
                if(textField_1.getText().isEmpty()){
                    JOptionPane.showMessageDialog(null, "Donneur indisponnible ",
                            "Attention", JOptionPane.WARNING_MESSAGE);
                }else {
                    try {
                        int rep = JOptionPane.showConfirmDialog(
                                null,
                                "Voulez-vous Modifier?",
                                "Confirmation",
                                JOptionPane.INFORMATION_MESSAGE);
                        if(rep==0){
                            ResultSet rs;
                            rs = Conn.createStatement().executeQuery(Query);
                            String num = textField_1.getText();
                            Conn.createStatement().executeQuery("Commit");
                            JOptionPane.showMessageDialog(null, "Donneur N�"+num+" sera modifier",
                                    "Infromation", JOptionPane.INFORMATION_MESSAGE);
                        }else{
                            JOptionPane.showMessageDialog(
                                    null,
                                    "modification annulee",
                                    "Information",
                                    JOptionPane.INFORMATION_MESSAGE
                            );
                        }

                    } catch (Exception y) {

                        JOptionPane.showMessageDialog(null, "Impossible de modifier",
                                "Attention", JOptionPane.WARNING_MESSAGE);
                        y.printStackTrace();
                    }
                }
            }
        });

        supprimer.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String Query = "Delete from Donneur where NUM_DONNEUR='"+textField_1.getText()+"'";
                String Query2 = "Delete from utilisateurgl where email='"+textField_6.getText()+"'";
                if(textField_1.getText().isEmpty()){
                    JOptionPane.showMessageDialog(null, "Donneur indisponnible ",
                            "Attention", JOptionPane.WARNING_MESSAGE);
                }else{
                    try {
                        int rep = JOptionPane.showConfirmDialog(
                                null,
                                "Voulez-vous suuprimer?",
                                "Confirmation",
                                JOptionPane.INFORMATION_MESSAGE);
                        if(rep==0){
                            ResultSet rs;
                            ResultSet rs2;
                            String num = textField_1.getText();
                            rs = Conn.createStatement().executeQuery(Query);
                            rs2 = Conn.createStatement().executeQuery(Query2);
                            Conn.createStatement().executeQuery("Commit");
                            JOptionPane.showMessageDialog(null, "Donneur N�"+num+" sera supprimee",
                                    "Infromation", JOptionPane.INFORMATION_MESSAGE);
                            Init();
                        }else{
                            JOptionPane.showMessageDialog(
                                    null,
                                    "Suppression annulee",
                                    "Information",
                                    JOptionPane.INFORMATION_MESSAGE
                            );
                        }

                    }catch(Exception y) {

                        JOptionPane.showMessageDialog(null, "Impossible de supprimer",
                                "Attention", JOptionPane.WARNING_MESSAGE);
                        y.printStackTrace();
                    }
                }
            }
        });

        annuler.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Init();
            }
        });

        recherche_donneur.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                recherche_donneur.setText("");
            }
        });
        rechercher.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String Query = "select * from Donneur where NUM_DONNEUR='"+recherche_donneur.getText()+"'";
                try {
                    ResultSet rs;
                    rs = Conn.createStatement().executeQuery(Query);
                    if(rs.next()){
                        ajouter.setEnabled(false);
                        modifier.setEnabled(true);
                        supprimer.setEnabled(true);
                        textField_1.setText(rs.getString(1));
                        textField_2.setText(rs.getString(2));
                        textField_3.setText(rs.getString(3));
                        textField_4.setText(rs.getString(4));
                        textField_5.setText(rs.getString(5));
                        textField_6.setText(rs.getString(6));
                        textField_7.setText(rs.getString(7));
                        textField_8.setText(rs.getString(8));
                        liste_sang.setSelectedItem((String)rs.getString(9));
                        liste_rh.setSelectedItem((String)rs.getString(10));
                        liste_genre.setSelectedItem((String)rs.getString(11));
                    }else{
                        ajouter.setEnabled(true);
                        modifier.setEnabled(false);
                        supprimer.setEnabled(false);
                        JOptionPane.showMessageDialog(null, "Donneur indisponnible ",
                                "Attention", JOptionPane.WARNING_MESSAGE);

                    }
                }catch(Exception y) {
                    y.printStackTrace();}
            }
        });
        this.setVisible(false);

    }

    private void Init(){
        textField_1.setText("");
        textField_2.setText("");
        textField_3.setText("");
        textField_4.setText("");
        textField_5.setText("");
        textField_6.setText("");
        textField_7.setText("");
        textField_8.setText("");
        liste_sang.setSelectedIndex(0);
        liste_rh.setSelectedIndex(0);
        liste_genre.setSelectedIndex(0);
        ajouter.setEnabled(true);
        modifier.setEnabled(true);
        supprimer.setEnabled(true);
    }

}
