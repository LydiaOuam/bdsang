package com.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Connexion {
    private String url = "jdbc:oracle:thin:@//localhost:1521/xe";
    private String passwd = "projetgl2";
    private String user ="projetgl2";
    private Connection connecter = null;
    private String Query;


    public String getUrl() {return this.url;}
    public String getUser() {return this.user;}
    public String getPasswd() {return this.passwd;}
    public String getQuery() {return this.Query;}
    public void setUrl(String url) {this.url = url;}
    public void setUser(String user) {this.user = user;}
    public void setPasswd(String passwd){this.passwd = passwd;}
    public void setQuery(String Query) {this.Query = Query;}


    public Connexion(String User, String passwd,Connection conn){
        this.user = User;
        this.passwd = passwd;
        this.connecter = conn;
    }


    public Connection Connecter()  throws SQLException, ClassNotFoundException {
        Class.forName("oracle.jdbc.driver.OracleDriver");
        this.connecter = DriverManager.getConnection(url, user, passwd);
        return this.connecter;
    }

    public ResultSet ExecuterReq(String query)throws SQLException {
        ResultSet rs = null;
        this.connecter.createStatement().executeUpdate(query);
        this.connecter.createStatement().executeUpdate("commit");
        return rs;
    }

    public void CreeBD() throws SQLException {
        Connection conn = this.connecter;
        conn.createStatement().executeUpdate("CREATE TABLE DONNEUR(" +
                "    NUM_DONNEUR NUMBER(5) PRIMARY KEY," +
                "    NOM VARCHAR2(30)," +
                "    PRENOM  VARCHAR2(30)," +
                "    ADRESSE VARCHAR2(50)," +
                "    NUM_TEL integer," +
                "    Adresse_mail VARCHAR(20) unique," +
                "    REMARQUE VARCHAR(20)," +
                "    DATE_NAISSANCE VARCHAR2(20)," +
                "    GROUPE_SANGUIN VARCHAR2(4)," +
                "    RHESUS CHAR," +
                "    genre VARCHAR(20), " +
                "    image varchar(50)," +
                "    CHECK  (GROUPE_SANGUIN IN('A','B','AB','O'))," +
                "    CHECK  (RHESUS IN('-','+'))" +
                ")");
        conn.createStatement().executeUpdate("CREATE TABLE UTILISATEURGL(" +
                "    email VARCHAR(20) PRIMARY key," +
                "    password VARCHAR(20)," +
                "    type_user VARCHAR(20)" +
                ")");
        conn.createStatement().executeUpdate("CREATE TABLE DEMANDE_RV(" +
                "    NUM_DONNEUR NUMBER(5) REFERENCES DONNEUR ON DELETE CASCADE," +
                "    DATE_RV VARCHAR2(20)," +
                "    Heure VARCHAR2(6)," +
                "    etat Varchar(20) DEFAULT 'Acceptee'," +
                "    PRIMARY KEY (NUM_DONNEUR,DATE_RV)" +
                ")");
        conn.createStatement().executeUpdate("CREATE TABLE MESSAGE("+
                "NUM_EMETTEUR VARCHAR2(30) ,"+
                "DATE_MESSAGE VARCHAR2(20),"+
                "CONTENU_MESSAGE  CHAR(500),"+
                "NUM_RECEPTEUR NUMBER(3) REFERENCES Donneur(Num_donneur) ON DELETE CASCADE,"+
                "PRIMARY KEY( NUM_EMETTEUR,NUM_RECEPTEUR,DATE_MESSAGE)"+
                ")");
        conn.createStatement().executeUpdate("CREATE TABLE NOTIFICATION ("+
                "NUM_EMETTEUR VARCHAR2(30) , "+
                "DATE_NOTIF VARCHAR2(20),"+
                "CONTENU_NOTIF  VARCHAR2(100),"+
                "PRIMARY KEY( NUM_EMETTEUR,DATE_NOTIF)"+
                ")");
        conn.createStatement().executeUpdate("CREATE TABLE ALERTE(" +
                "    NUM_EMETTEUR VARCHAR2(30) ," +
                "    DATE_ALERTE VARCHAR2(20)," +
                "    CONTENU_ALERTE  VARCHAR2(100)," +
                "    PRIMARY KEY( NUM_EMETTEUR,DATE_ALERTE)" +
                ")");
        conn.createStatement().executeUpdate("Create Table Forum( " +
                "    Message VARCHAR(500), " +
                "    NUM_DONNEUR NUMBER(5) REFERENCES DONNEUR ON DELETE CASCADE, " +
                "    DATE_Message VARCHAR2(30), " +
                "    PRIMARY KEY( NUM_DONNEUR,DATE_Message) " +
                ")");
        conn.createStatement().executeUpdate("commit");
    }

    public void AjouterDonnees() throws SQLException{
        Connection conn = this.connecter;
        conn.createStatement().executeUpdate("INSERT INTO DONNEUR VALUES(1,'Rehman','Belaid','ALGER','098738387','rehmanb@gmail.com','RIEN','01/01/2000','A','-','Homme','Aucune')");
        conn.createStatement().executeUpdate("INSERT INTO DONNEUR VALUES(2,'Tiki','Fatima Zahra','ALGER','0763537229','tikifz@gmail.com','RIEN','01/01/1995','O','+','Femme','Aucune')");
        conn.createStatement().executeUpdate("INSERT INTO UTILISATEURGL VALUES('tikifz@gmail.com','0000','Donneur')");
        conn.createStatement().executeUpdate("INSERT INTO UTILISATEURGL VALUES('rehmanb@gmail.com','0000','Donneur')");
        conn.createStatement().executeUpdate("INSERT INTO UTILISATEURGL VALUES('user@gmail.com','1111','Admin')");
        conn.createStatement().executeUpdate("INSERT INTO DEMANDE_RV VALUES(1,'25/03/2021','12h20','Acceptee')");
        conn.createStatement().executeUpdate("INSERT INTO DEMANDE_RV VALUES(1,'02/01/2020','18h45','Acceptee')");
        conn.createStatement().executeUpdate("INSERT INTO DEMANDE_RV VALUES(1,'02/07/2020','8h15','Refusee')");
        conn.createStatement().executeUpdate("INSERT INTO DEMANDE_RV VALUES(2,'02/01/2020','16h25','Refusee')");
        conn.createStatement().executeUpdate("INSERT INTO DEMANDE_RV VALUES(2,'12/04/2017','10h05','Attente')");
        conn.createStatement().executeUpdate("INSERT INTO DEMANDE_RV VALUES(2,'19/10/2019','14h13','Attente')");
        conn.createStatement().executeUpdate("INSERT INTO MESSAGE VALUES(1,'28/02/2019','Bonjour,comment ca va?',2)");
        conn.createStatement().executeUpdate("INSERT INTO MESSAGE VALUES(2,'28/02/2019','Ca va bien et toi?',1)");
        conn.createStatement().executeUpdate("INSERT INTO NOTIFICATION VALUES('user@gmail.com','24/03/2021','Rappel: Votre Rendez-Vous est prevu demain')");
        conn.createStatement().executeUpdate("INSERT INTO NOTIFICATION VALUES('user@gmail.com','27/03/2021','Votre don a ete bien enregitre')");
        conn.createStatement().executeUpdate("INSERT INTO NOTIFICATION VALUES('user@gmail.com','01/01/2020','Rappel: Votre Rendez-Vous est prevu demain')");
        conn.createStatement().executeUpdate("INSERT INTO NOTIFICATION VALUES('user@gmail.com','10/04/2017','L''organisation des dons prevu pour le 11 est annulee')");
        conn.createStatement().executeUpdate("INSERT INTO NOTIFICATION VALUES('user@gmail.com','18/10/2019','Rappel: Votre Rendez-Vous est prevu demain')");
        conn.createStatement().executeUpdate("INSERT INTO ALERTE VALUES('user@gmail.com','03/01/2020','Vous avez rate votre rendez vous')");
        conn.createStatement().executeUpdate("INSERT INTO Forum VALUES('Bonjour,C''est pour quand la prochaine organisation des dons?',1,'28/02/2019')");
        conn.createStatement().executeUpdate("INSERT INTO Forum VALUES('Bonjour,y''a t-il encore des rdv',2,'05/02/2017')");
        conn.createStatement().executeUpdate("Commit");
    }
}

