package com.company;

import net.proteanit.sql.DbUtils;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;

public class GestionDonneur extends JPanel {


    private JTable donneurs = new JTable();
    private JScrollPane DB_donneur = new JScrollPane(donneurs);


    private JComboBox filtre_avec_sang = new JComboBox(),filtre_avec_rh = new JComboBox(),
                      liste_rh = new JComboBox(),liste_sang = new JComboBox(),
                      critaire = new JComboBox();

    private JTextField donner = new JTextField();
    private JButton filtre = new JButton("Filtre"),ajouter =new JButton("Ajouter"),
            modifier = new JButton("Modifier"),supprimer =new JButton("Supprimer");


    public GestionDonneur(int x, int y, int longueur, int hauteur, Connection Conn) {
        try {
            ResultSet rs;
            rs = Conn.createStatement().executeQuery("select * from DONNEUR");
            donneurs.setModel(DbUtils.resultSetToTableModel(rs));

        }catch(Exception e) {
            e.printStackTrace();}
        setSize( longueur,hauteur);
        this.setLayout(null) ;
        int xx=40,yy=150;

        JLabel lblNewLabel = new JLabel("<html><h1><u>Liste Donneurs</u></h1></html>");
        lblNewLabel.setBounds((longueur/2)-100, 30, 200, 80);
        lblNewLabel.setFont(new java.awt.Font("Monospaced", 1, 40));
        this.add(lblNewLabel);

        this.add(DB_donneur);
        this.DB_donneur.setBounds(xx,yy,850,400);

        this.add(liste_sang);
        this.add(liste_rh);

        this.add(filtre);
        this.filtre.setBounds(xx+360,yy-40,150,30);
        this.filtre.setBackground(Color.WHITE);
        this.filtre.setForeground(Color.BLACK);

        this.add(filtre_avec_sang);
        this.filtre_avec_sang.setBounds(xx,yy-40,100,30);
        filtre_avec_sang.setModel(new DefaultComboBoxModel(new String[]{"aucun","A", "B", "AB", "O"}));

        this.add(filtre_avec_rh);
        this.filtre_avec_rh.setBounds(xx+120,yy-40,100,30);
        filtre_avec_rh.setModel(new DefaultComboBoxModel(new String[]{"aucun","+","-"}));


//        this.add(critaire);
//        this.critaire.setBounds(xx+240,yy-40,100,30);
//        critaire.setModel(new DefaultComboBoxModel(new String[]{"aucun","age","adresse"}));

        filtre.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String Query= "select * from DONNEUR";
                if((String)filtre_avec_sang.getSelectedItem() != "aucun"){
                    Query = "select * from DONNEUR where GROUPE_SANGUIN='"+(String)filtre_avec_sang.getSelectedItem()+"'";
                }
                if((String)filtre_avec_rh.getSelectedItem() != "aucun"){
                    Query = "select * from DONNEUR where RHESUS='"+(String)filtre_avec_rh.getSelectedItem()+"'";
                }

                if((String)filtre_avec_rh.getSelectedItem() != "aucun"
                        && (String)filtre_avec_sang.getSelectedItem() != "aucun"){
                    Query = "select * from DONNEUR where RHESUS='"+(String)filtre_avec_rh.getSelectedItem()+"' and " +
                            "GROUPE_SANGUIN='"+(String)filtre_avec_sang.getSelectedItem()+"'";
                }
                try {
                    ResultSet rs;
                    rs = Conn.createStatement().executeQuery(Query);
                    donneurs.setModel(DbUtils.resultSetToTableModel(rs));
                    Query= "select * from DONNEUR";

                }catch(Exception y) {
                    y.printStackTrace();}
            }
        });
        DB_donneur.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }
        });
        setVisible(true);
    }
}
