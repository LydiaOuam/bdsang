package com.company;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class MessagePan extends JPanel {
    //int sizeL;int sizeH;String expediteur;String message;String heure;boolean vu;
    String Message;

    public String getMessage() {
        return Message;
    }

    public MessagePan(int sizeL, int sizeH, String expediteur, String message, String heure, boolean vu){
        this.setLayout(null);
        this.setSize(sizeL,sizeH);

        JLabel exp = new JLabel(expediteur),hr = new JLabel(heure),
                msg = new JLabel(message.length()<=30? message : message.substring(0,30)+"...");
        this.Message = "\nEnvoyer par: "+expediteur+"\n\nHeure : "+heure+"\n\nMessage:\n"+message+"";
        this.add(exp);
        exp.setBounds(10,10,120,15);
        this.add(msg);
        msg.setBounds(10,25,sizeL,20);
        msg.setForeground(Color.GRAY);

        this.add(hr);
        hr.setBounds(250,25,150,20);
        hr.setForeground(Color.GRAY);
        //this.setBorder(new LineBorder(Color.BLACK));
    }
}
