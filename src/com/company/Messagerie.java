package com.company;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;

public class Messagerie extends JPanel {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel titre = new JLabel("<html><h1><u>Messagerie</u></h1></html>");

    private JPanel pan = new JPanel();
    private JScrollPane panMsg = new JScrollPane(pan,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER) ;


    private JPanel titre_ = new JPanel();

    private JTextArea messagerecu = new JTextArea();
    private JScrollPane pan_msg_recu = new JScrollPane(messagerecu);

    private JButton actualiser = new JButton("Actualiser");

    public Messagerie(int x, int y, int longueur, int hauteur, Connection Conn, String num_donneur) {
        messagerecu.setForeground(Color.BLACK);
        messagerecu.setEditable(false);
        messagerecu.setLineWrap(true); // pour retour a  la ligne automatique
        messagerecu.setWrapStyleWord(true);// pour ne pas couper les mots
        AffichageMSG(Conn,num_donneur);

        setSize(longueur, hauteur);
        this.setLayout(null);
        this.add(titre);
        this.titre.setBounds((longueur / 2)-50, 20, 600, 90);
        this.titre.setFont(new java.awt.Font("Monospaced", 1, 40));

        this.add(titre_);
        this.titre_.add( new JLabel("Messages"));

        this.titre_.setBounds(0,100,400,30);
        this.titre_.setBorder(new LineBorder(Color.BLACK));

        this.add(panMsg);
        this.panMsg.setBounds(0,130,400,hauteur-130);

        pan.setPreferredSize(new Dimension(this.getWidth(), 50*25));
        this.pan.setLayout((null));


        this.add(pan_msg_recu);
        this.pan_msg_recu.setBounds(400,100,longueur-400,hauteur-100);
        this.pan_msg_recu.setBorder(new LineBorder(Color.BLACK));

        this.add(actualiser);
        this.actualiser.setBounds(longueur-150,70,150,30);
        this.actualiser.setBorder(new LineBorder(Color.BLACK));
        this.actualiser.setBackground(Color.WHITE);
        this.actualiser.setForeground(Color.BLACK);
        actualiser.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                AffichageMSG(Conn,num_donneur);
            }
        });

    }

    private void AffichageMSG(Connection Conn,String num_donneur){
        ResultSet rs = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        ResultSet rs4 = null;
        try {
            String Query ="select * from message where NUM_RECEPTEUR="+num_donneur;
            rs = Conn.createStatement().executeQuery(Query);
           

            int i = 0;
            while(rs.next()){

                MessagePan xx = new MessagePan(400,60,rs.getString("NUM_EMETTEUR"),
                        rs.getString("CONTENU_MESSAGE"),
                        rs.getString("DATE_MESSAGE"),false);
                

                this.pan.add(xx);
                xx.setBounds(0,60*i,400,60);
                int finalI = i;
                xx.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        messagerecu.setText(xx.getMessage());
                    }
                });
                i++;
            }
//Alertes
            String Query2 ="select * from alerte";
            rs2 = Conn.createStatement().executeQuery(Query2);
            while(rs2.next()){
                MessagePan xx = new MessagePan(400,60,rs2.getString("NUM_EMETTEUR"),
                		rs2.getString("CONTENU_ALERTE"), 
                		rs2.getString("DATE_ALERTE"),false);  

                this.pan.add(xx);
                xx.setBounds(0,60*i,400,60);
                int finalI = i;
                xx.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        messagerecu.setText(xx.getMessage());
                    }
                });
                i++;
            }
            
///Notifications
            String Query3 ="select * from notification";
            rs3 = Conn.createStatement().executeQuery(Query3);
            while(rs3.next()){
                MessagePan xx = new MessagePan(400,60,rs3.getString("NUM_EMETTEUR"),
                		rs3.getString("CONTENU_NOTIF"), 
                		rs3.getString("DATE_NOTIF"),false);
                this.pan.add(xx);
                xx.setBounds(0,60*i,400,60);
                int finalI = i;
                xx.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        messagerecu.setText(xx.getMessage());
                    }
                });
                i++;
            }
            pan.setPreferredSize(new Dimension(400,60*(i-1)));
        } catch (Exception r) {
        	r.printStackTrace();
            JOptionPane.showMessageDialog(null,"Impossible de modifier" ,
                    "Attention", JOptionPane.WARNING_MESSAGE);
        }

    }
}
