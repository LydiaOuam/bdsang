package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.util.ArrayList;

public class FenetrePrincipale extends JFrame {

    private JButton maj_etudiant = new JButton("Etudiant"),maj_module= new JButton("Module"),
                maj_examen= new JButton("Examen"),
            maj_etat= new JButton("Etat"),fleche = new JButton(new ImageIcon("img/fleche+.png"));
/*
=>gestion donneurs
• Répertorier les donneurs
• Classer les donneurs selon leurs caractéristiques

=> panneau donneurs
• Demande de RV : Pour faire un don bénévole
• Avoir Mini carnet de santé : leur permettant de suivre leur état de santé


=> communication
• Contacter les donneurs
• Envoyer des notifications
• Envoyer des alertes
• Communauté : Forum des donneurs
• Messages : Pour la réception de notifications frome le centre respectif.


*/
    private JButton  demande_rv = new JButton("Rendez-Vous")
                    ,donneurs = new JButton("Donneurs")
                    ,carnet_sante = new JButton("Carnet de sante")
                    ,communication = new JButton("Contacter")
                    ,mon_compte = new JButton("Mon Compte")
                    ,forum = new JButton("Forum")
                    ,messages = new JButton("Messagerie")
                    ,Mise_jour = new JButton("Mise a jour")
                    ,demande = new JButton("Demandes Rendez-Vous");


    private JButton[] btn_donneur = {mon_compte,carnet_sante,demande_rv,messages,forum};
    private JButton[] btn_admin = {donneurs,Mise_jour,demande,communication};
    private JButton[] menuButton;


    private JPanel menu = new JPanel();

    private ArrayList<JPanel> panneaux = new ArrayList<JPanel>();
    private JLabel txt1 = new JLabel("Etudiant"),txt2 = new JLabel("Etat"),
            txt3 = new JLabel("Module"),txt4 = new JLabel("Enseignant"),
            titre = new JLabel("<html><h3>Gestion donneurs</h3></html>");
    private ImageIcon image2 = new ImageIcon("Img/enseignant.jpg");
    private JLabel image_logo = new JLabel(image2);

    private void setTitre(String titre){this.setTitle(titre);}

    private void Quitter(){this.dispose();}
    private void Reduire(){this.setState(Frame.ICONIFIED);}
    void SetMenu(int x,int hauteur){
        this.add(menu);
        menu.setBounds(0,0,x,hauteur);
    }
    private ImageIcon quit = new ImageIcon("img/exit.png");
    private ImageIcon reduire_ico = new ImageIcon("img/reduire.png");
    private JButton fermer = new JButton(quit);
    private JButton reduire = new JButton(reduire_ico);
    Image ic = Toolkit.getDefaultToolkit().createImage("img/icon.png");

    private JPanel maj_donneurs,gestion,contacte,PDem,compte,msg_donneur,PCanet,rendezVous,Pforum;
    public FenetrePrincipale(String num_user,Connection Conn) {


        this.setIconImage(new ImageIcon("img/icon1.png").getImage());
        getContentPane().setLayout(null);
        this.setUndecorated(true);
        this.setTitle("Gestion - Etudiants");
        Dimension tailleMoniteur = Toolkit.getDefaultToolkit().getScreenSize();
        int longueur = tailleMoniteur.width * 5/6 ;
        int hauteur = tailleMoniteur.height * 5/6 ;
        this.setSize(longueur,hauteur+30);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.getContentPane().setBackground(Color.WHITE);

        this.getContentPane().add(fermer);
        this.fermer.setBounds(longueur-30,0,30,30);
        this.fermer.setBackground(Color.WHITE);
        this.fermer.setForeground(Color.BLACK);
        this.fermer.setBorder(null);

        this.getContentPane().add(reduire);
        this.reduire.setBounds(longueur-60,0,30,30);
        this.reduire.setBackground(Color.WHITE);
        this.reduire.setForeground(Color.BLACK);
        this.reduire.setBorder(null);

        this.getContentPane().add(titre);
        this.titre.setBounds((longueur/2)-50,0,150,30);

        /************************************************verification Connexion*/
        if(num_user.length()<=2 || !(Integer.parseInt(num_user.substring(0,2))==-1)){

            compte = new CompteDonneur(200,30,longueur-200,hauteur,num_user,Conn);
            compte.setBounds(200,30,longueur-200,hauteur);
            this.add(compte);
            compte.setVisible(false);
            panneaux.add(compte);

            msg_donneur = new Messagerie(200,30,longueur-200,hauteur,Conn,num_user);
            msg_donneur.setBounds(200,30,longueur-200,hauteur);
            this.add(msg_donneur);
            msg_donneur.setVisible(false);
            panneaux.add(msg_donneur);

            PCanet = new CarnetSante(200,30,longueur-200,hauteur,num_user,Conn);
            PCanet.setBounds(200,30,longueur-200,hauteur);
            this.add(PCanet);
            PCanet.setVisible(false);
            panneaux.add(PCanet);

            rendezVous = new RendezVous(200,30,longueur-200,hauteur,Conn,num_user);
            rendezVous.setBounds(200,30,longueur-200,hauteur);
            this.add(rendezVous);
            rendezVous.setVisible(false);
            panneaux.add(rendezVous);

            Pforum = new Forum(200,30,longueur-200,hauteur,num_user,Conn);
            Pforum.setBounds(200,30,longueur-200,hauteur);
            this.add(Pforum);
            Pforum.setVisible(false);
            panneaux.add(Pforum);


            menuButton = btn_donneur;
            panneaux.get(panneaux.indexOf(compte)).setVisible(true);

        }else{maj_donneurs = new MajDonneurs(200,30,longueur-200,hauteur,Conn);
            maj_donneurs.setBounds(200,30,longueur-200,hauteur);
            this.add(maj_donneurs);
            maj_donneurs.setVisible(false);
            panneaux.add(maj_donneurs);

            gestion = new GestionDonneur(200,30,longueur-200,hauteur,Conn);
            gestion.setBounds(200,30,longueur-200,hauteur);
            this.add(gestion);
            gestion.setVisible(false);
            panneaux.add(gestion);

            contacte = new Contacter(200,30,longueur-200,hauteur,Conn,
                    num_user.substring(1,num_user.length()));

            contacte.setBounds(200,30,longueur-200,hauteur);
            this.add(contacte);
            contacte.setVisible(false);
            panneaux.add(contacte);

            PDem = new DemandeRV(200,30,longueur-200,hauteur,Conn);
            PDem.setBounds(200,30,longueur-200,hauteur);
            this.add(PDem);
            PDem.setVisible(false);
            panneaux.add(PDem);

            menuButton =btn_admin;
            panneaux.get(panneaux.indexOf(maj_donneurs)).setVisible(true);}






        menu.setLayout(new GridLayout(10,1));
        menu.setBackground(Color.WHITE);

        for(JButton x:menuButton) {
            menu.add(x);
            x.setBackground(Color.WHITE);

            x.setBorder(null);
        }
        carnet_sante.setBackground(new Color(238,238,238));
        this.maj_etudiant.setBackground(Color.GRAY);
        JButton quitter = new JButton("Deconnecter");
        quitter.setBackground(Color.WHITE);
        quitter.setBorder(null);
        menu.add(quitter);


        this.getContentPane().add(menu);

        menu.setBounds(0,30,200,hauteur);

        carnet_sante.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                for(int i=0;i<panneaux.size();i++) {
                    if(panneaux.get(i).isVisible()) {
                        panneaux.get(i).setVisible(false);
                    }
                }

                PCanet.setVisible(true);
            }
        });

        demande_rv.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                for(int i=0;i<panneaux.size();i++) {
                    if(panneaux.get(i).isVisible()) {
                        panneaux.get(i).setVisible(false);
                    }
                }
                for(JButton x:menuButton) {
                    x.setBackground(Color.WHITE);
                }
                demande_rv.setBackground(null);
                rendezVous.setVisible(true);
            }
        });

       

        mon_compte.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                for(int i=0;i<panneaux.size();i++) {
                    if(panneaux.get(i).isVisible()) {
                        panneaux.get(i).setVisible(false);
                    }
                }
                for(JButton x:menuButton) {
                    x.setBackground(Color.WHITE);
                }
                mon_compte.setBackground(Color.red);
                compte.setVisible(true);
            }
        });


        donneurs.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                for(int i=0;i<panneaux.size();i++) {
                    if(panneaux.get(i).isVisible()) {
                        panneaux.get(i).setVisible(false);
                    }
                }
                for(JButton x:menuButton) {
                    x.setBackground(Color.WHITE);
                }
                gestion.setVisible(true);
            }
        });

        Mise_jour.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                for(int i=0;i<panneaux.size();i++) {
                    if(panneaux.get(i).isVisible()) {
                        panneaux.get(i).setVisible(false);
                    }
                }
                for(JButton x:menuButton) {
                    x.setBackground(Color.WHITE);
                }
                maj_donneurs.setVisible(true);
            }
        });


        communication.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                for(int i=0;i<panneaux.size();i++) {
                    if(panneaux.get(i).isVisible()) {
                        panneaux.get(i).setVisible(false);
                    }
                }
                for(JButton x:menuButton) {
                    x.setBackground(Color.WHITE);
                }
                contacte.setVisible(true);
            }
        });
        forum.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                for(int i=0;i<panneaux.size();i++) {
                    if(panneaux.get(i).isVisible()) {
                        panneaux.get(i).setVisible(false);
                    }
                }
                for(JButton x:menuButton) {
                    x.setBackground(Color.WHITE);
                }
                Pforum.setVisible(true);
            }
        });

        messages.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                for(int i=0;i<panneaux.size();i++) {
                    if(panneaux.get(i).isVisible()) {
                        panneaux.get(i).setVisible(false);
                    }
                }
                for(JButton x:menuButton) {
                    x.setBackground(Color.WHITE);
                }
                msg_donneur.setVisible(true);
            }
        });

        demande.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                for(int i=0;i<panneaux.size();i++) {
                    if(panneaux.get(i).isVisible()) {
                        panneaux.get(i).setVisible(false);
                    }
                }
                for(JButton x:menuButton) {
                    x.setBackground(Color.WHITE);
                }
                PDem.setVisible(true);
            }
        });
        quitter.addMouseListener(
                new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Quitter();
                Authentification x = new Authentification(Conn);
            }
        });
        fermer.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Quitter();
            }
        });

        reduire.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Reduire();
            }
        });
        this.setVisible(true);

    }



}
